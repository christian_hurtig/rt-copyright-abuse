import re

class RTTicket(object):
	def __init__(self, id):
		self.id = id
		self.attachmentId = -1
		self.title = ""	
		
	def setAttachmentId(self, rawAttachmentId):
		attachmentIdString = self.parseXMLFromAttchmentId(rawAttachmentId)
		if(attachmentIdString is None):
			return 0
		self.attachmentId = int(attachmentIdString)
		return 1
		
	def setTitle(self, rawTitleTagContents):
		titleString = self.parseTitleTagContents(rawTitleTagContents)
		if(titleString is None):
			return 0
		self.title = titleString
		return 1		
		
	def __str__(self):
		return 	"Ticket id:" + str(self.id) + ", attachment Id:" + str(self.attachmentId) + ", Title:" + self.title
	
	@staticmethod
	def parseXMLFromAttchmentId(attachmentIdText):
		pattern = re.compile("\d+(?=: [0-9A-z]*\.xml)", re.MULTILINE)
		return pattern.search(attachmentIdText).group(0)
	
	@staticmethod
	def parseTitleTagContents(rawAttachmentText):
		pattern = re.compile("(?<=\<Title\>).*(?=\<\/Title\>)", re.MULTILINE)
		return pattern.search(rawAttachmentText).group(0)