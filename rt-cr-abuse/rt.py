import requests
import urllib


'''
	A class handling connections and http requests to a request tracker server.
'''
class Connector(object):
	def __init__(self, host, username, password):
		self.host = host
		self.user = username
		self.password = password
		self.credentials = { 'user': self.user, 'pass': self.password}
	
	'''
	Get ids of the copyright abouse tickets created before the specified date

	:param date startDate: The date specifing the earliest possible ticket creation date.
	:return: ids of the requested tickets.
	:rtype: string[]
	'''
	def getAbuseTicketIds(self, startDate):
		rawTicketIds = requests.get(self.host + "/REST/1.0/search/ticket?query=(Queue='Abuse-copyright')AND(Created>='" + startDate + "')&orderby=-Created", params=self.credentials)
		self.checkRequest(rawTicketIds)
		return rawTicketIds
	
	'''
	Gets the contents of the specified ticket attachment.

	:param int ticketId: The id of an existing support ticket.
	:param int attachmentId: The id of an existing attachment connected to the specified ticket.
	:return: The contents of the specified attachment.
	:rtype: string
	'''
	def getAttachmentContent(self, ticketId, attachmentId):
		rawAttachmentContent = requests.get(self.host + "/REST/1.0/ticket/"+ str(ticketId) +"/attachments/"+ str(attachmentId) +"/content", params=self.credentials)
		self.checkRequest(rawAttachmentContent)
		return rawAttachmentContent
		
	def getAttachmentIds(self, ticketId):
		if(ticketId == -1):
			return None
		rawAttachmentIds = requests.get(self.host + "/REST/1.0/ticket/" + str(ticketId) + "/attachments", params=self.credentials)
		self.checkRequest(rawAttachmentIds)
		return rawAttachmentIds
		
	def logout(self):
		r = post("https://" + self.host + "REST/1.0/logout", params.credentials, data=" ")
		return r.text
	
	@staticmethod
	def checkRequest(Request):
		if (Request.status_code != 200):
			print(ticketIdRequest.status_code + ": " + ticketIdRequest.reason)
			raise SystemExit(0)