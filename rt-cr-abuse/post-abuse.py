from rt import Connector
from ticket import RTTicket
from datetime import date, timedelta
import re

'''
   Get date a specified number of months back. The day portion of date is always the first.

   :param int monthsBack: Number of months from today
   :return: the requested date
   :rtype: date
'''
def getDateMonthsBack(monthsBack):
	retDate = date.today().replace(day=1)
	while monthsBack > 1:
		retDate = retDate - timedelta(days=1)
		retDate = retDate.replace(day=1)
		monthsBack -= 1
		
	print(retDate)
	return retDate

def parseAbuseIds(ticketIdText):
	pattern = re.compile("^\d+", re.MULTILINE)
	return pattern.findall(ticketIdText)

host = '***'
rtUsername = '***'
rtPassword = '***'
ticketList = []

rt = Connector(host, rtUsername, rtPassword)
targetDate = getDateMonthsBack(0)
dateString = targetDate.strftime('%Y-%m-%d')

rawticketIds = rt.getAbuseTicketIds(dateString)
AbuseIds = parseAbuseIds(rawticketIds.text)

for id in AbuseIds:
	ticketList.append(RTTicket(int(id)))
	
for ticket in ticketList:
	rawAttachmentId = rt.getAttachmentIds(ticket.id)
	ticket.setAttachmentId(rawAttachmentId.text)
	
for ticket in ticketList:
	rawAttachmentContent = rt.getAttachmentContent(ticket.id, ticket.attachmentId)
	ticket.setTitle(rawAttachmentContent.text)
	
for ticket in ticketList:
	print(ticket)